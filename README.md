# e9a-bsh-webpage 1.0
Deutsch: Auf dieser kleinen Webpage sollen sich übersichtlich und schnell Informationen bzw. der Speiseplan abrufen können.
## Version 1.0 - nach einer langen, anstrengenden Beta-Phase ist die aktuelle Version nun auf https://bsh.e9a.at erreichbar.
## ToDo
* [X] Telegram-Bot Info-Page
* ~~[ ] Schülervertretung: Aktuelle Infos, Sitzungsprotokolle, evtl. Umfragen~~
* [X] Mainpage: Automatische Anzeige des aktuellen und zukünftigen Speiseplans
* [ ] Effizientere Aktualisierung
* [X] Kompatiblität zu Smartphones
* [X] Hardcoded Pixel entfernen
* [X] Design
* [X] HTTPS-Caching: Derzeit wird der Speiseplan nach 12h aktualisiert.
