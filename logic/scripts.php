<?php
/**
 * Created by PhpStorm.
 * User: dafnik
 * Date: 11.12.17
 * Time: 22:48
 */
?>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
    var cookie = document.cookie;
    if(cookie !== "isDismissed=true") {
        document.getElementById("hello_alert").innerHTML =
            '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">\n' +
            '     <div class="alert alert-info alert-dismissible" role="alert">\n' +
            '       <button onclick="onClickDismiss()" type="button" class="close" data-dismiss="alert" aria-label="Close">' +
            '           <span aria-hidden="true">&times;</span>' +
            '       </button>' +
            '       <strong>Hello!</strong> Hier siehst du eine Übersicht aller Speisepläne für diese und nächste Woche.' +
            '    </div>' +
            '</div>';
    }

    function onClickDismiss() {
        document.cookie = "isDismissed=true";
    }
</script>