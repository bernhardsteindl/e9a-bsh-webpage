<?php
/*Hardcoded Links*/
$thisWeek="http://www.bshkrems.at/download/Speiseplan_a.pdf";
$nextWeek="http://www.bshkrems.at/download/Speiseplan_b.pdf";
/*Path to PDFs*/
$menuDir="menu";
$pathMenuA=$menuDir."/thisWeekMenu.pdf";
$pathMenuB=$menuDir."/nextWeekMenu.pdf";

$cacheTime=43200;
if (!file_exists($menuDir))
{
    mkdir($menuDir);
}
if (!file_exists($pathMenuA) or (time()- filemtime($pathMenuA)>=$cacheTime) or $_REQUEST['force-reload'])
{
    file_put_contents($pathMenuA, file_get_contents($thisWeek));
    file_put_contents($pathMenuB, file_get_contents($nextWeek));
}
?>